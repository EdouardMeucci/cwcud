<?php

function arrayRegions() {

    $region = array();

    $region["84"] = "Auvergne-Rhône-Alpes";
    $region["27"] = "Bourgogne-France-Comté";
    $region["53"] = "Bretagne";
    $region["24"] = "Centre-Val de Loire";
    $region["94"] = "Corse";
    $region["44"] = "Grand Est";
    $region["32"] = "Haut-de-France";
    $region["11"] = "Île-de-France";
    $region["28"] = "Normandie";
    $region["75"] = "Nouvelle-Aquitaine";
    $region["76"] = "Occitanie";
    $region["52"] = "Pays-de-la-Loire";
    $region["93"] = "Provence-Alpes-Côte-d'Azur";
    $region["01"] = "Régions ultra-marines";

    return $region;

}

?>