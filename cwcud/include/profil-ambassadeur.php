<?php
use \Mailjet\Resources;
/**
 * Description: une extension pour gérer un profil ambassadeur en front
 * Author : I Have a Green
 * Author URI: https://ihaveagreen.fr/
 * Contributors: I Have a Green
 * Text Domain: w-profil-ambassadeur
 * Version: 0.1
 * Stable tag: 0.1
 */

/**
 * Bloquer les accès directs
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( esc_html__( 'Cheatin&#8217; uh?' ) );
}

add_action('rest_api_init', function() {
	
	register_rest_route( 'ihag', 'coordonate-ambassadeur',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'ihagCoordonateAmbassadeur',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
	
});

// wp_dashboard_setup is the action hook
add_action( 'wp_dashboard_setup', 'ihag_dashboard_new_ambassadeur' );
function ihag_dashboard_new_ambassadeur() {
	wp_add_dashboard_widget( 'ihag_dashboard_new_ambassadeur', 'Ambassadeur en attente', 'ihag_dashboard_new_ambassadeur_display' );
}
function ihag_dashboard_new_ambassadeur_display() {
	?>
        <ul>
            <?php
            $users = get_users('role=ambassadeur_to_validate');
            foreach ( $users as $user ) :
                echo '<li><a href="' . admin_url( 'user-edit.php?user_id=' . $user->ID ) . '">' . $user->first_name.' '.$user->last_name. '</a></li>';
            endforeach;
            ?>
        </ul>
        <a class="button" href="<?php echo admin_url();?>users.php?role=ambassadeur_to_validate">Tous les ambassadeurs en attente</a>
	<?php
}


add_action('init', 'w_demo_register_role_on_plugin_activation' );
function w_demo_register_role_on_plugin_activation() {
    add_role( 'ambassadeur', __( 'Ambassadeur', 'annuaire' ), array( 'read' => true, 'level_0' => true ) );
    add_role( 'ambassadeur_to_validate', __( 'Ambassadeur à valider', 'annuaire' ), array( 'read' => true, 'level_0' => true ) );
}

add_action('show_user_profile', 'ihag_custom_user_profile_fields_ambassadeur');
add_action('edit_user_profile', 'ihag_custom_user_profile_fields_ambassadeur');
function ihag_custom_user_profile_fields_ambassadeur( $user ) {
    if ( in_array( 'ambassadeur', $user->roles, true ) || in_array( 'ambassadeur_to_validate', $user->roles, true ) ) {
?>
    <h2>Ambassadeur</h2>
    <table class="form-table">
        <tr>
            <th>
                <label for="level_ambassadeur"><?php _e( 'Niveau ambassadeur' ); ?></label>
            </th>
            <td>
                <select name="level_ambassadeur" id="level_ambassadeur" >
                    <option value="" <?php selected( '', get_the_author_meta( 'level_ambassadeur', $user->ID ) ); ?>><?php _e('Niveau non attribué', 'cwcud');?></option>
                    <option value="national" <?php selected( 'national', get_the_author_meta( 'level_ambassadeur', $user->ID ) ); ?>><?php _e('Ambassadeur national', 'cwcud');?></option>
                    <option value="regional" <?php selected( 'regional', get_the_author_meta( 'level_ambassadeur', $user->ID ) ); ?>><?php _e('Ambassadeur régional', 'cwcud');?></option>
                    <option value="departemental" <?php selected( 'departemental', get_the_author_meta( 'level_ambassadeur', $user->ID ) ); ?>><?php _e('Ambassadeur départemental', 'cwcud');?></option>
                    <option value="local" <?php selected( 'local', get_the_author_meta( 'level_ambassadeur', $user->ID ) ); ?>><?php _e('Ambassadeur local', 'cwcud');?></option>
                </select>
            </td>
        </tr>
        
        <tr>
            <th>
                <label for="user_gender">Genre</label>
            </th>
            <td>
                <div class="form-row">
                    <div class="checkbox">
                        <input type="radio" name="user_gender" class="gender" id="user_female" value="user_female" <?php checked( get_user_meta($user->ID , 'user_gender', true ), "user_female");?> >
                        <label class="checkbox-label"  for="user_female"><?php _e('Madame', 'cwcud');?></label>
                    </div>
                    <div class="checkbox">
                        <input type="radio" name="user_gender" class="gender" id="user_male" value="user_male"  <?php checked( get_user_meta($user->ID , 'user_gender', true ), "user_male");?>>
                        <label class="checkbox-label" for="user_male"><?php _e('Monsieur', 'cwcud');?></label>
                    </div>
                </div>
            </td>
        </tr>

        <tr>
            <th><label for="user_phone">Téléphone</label></th>
            <td>
                <input
                    type="text"
                    value="<?php echo esc_attr(get_user_meta($user->ID, 'user_phone', true)); ?>"
                    name="user_phone"
                    id="user_phone"
                    class="regular-text"
                >
            </td>
        </tr>
        <tr>
            <th><label for="number_authorisation">Afficher numéro de téléphone</label></th>
            <td>
                <div class="form-row">
                    <div class="checkbox">
                        <input type="radio" name="number_authorisation" id="number_yes" class="number" value="number_yes"  <?php checked( get_user_meta($user->ID , 'number_authorisation', true ), "number_yes");?>>
                        <label class="checkbox-label" for="number_yes"><?php _e('Oui', 'cwcud');?></label>
                    </div>
                    <div class="checkbox">
                        <input type="radio" name="number_authorisation" id="number_no" class="number" value="number_no" <?php checked( get_user_meta($user->ID , 'number_authorisation', true ), "number_no");?>>
                        <label class="checkbox-label" for="number_no"><?php _e('Non', 'cwcud');?></label>
                    </div>
                </div>
            </td>
        </tr>
        

        <tr>
            <th><label for="user_adress">Adresse</label></th>
            <td>
                <input
                    type="text"
                    value="<?php echo esc_attr(get_user_meta($user->ID, 'user_adress', true)); ?>"
                    name="user_adress"
                    id="user_adress"
                    class="regular-text"
                >
            </td>
        </tr>
        <tr>
            <th><label for="user_code_postal">Code Postal</label></th>
            <td>
                <input
                    type="text"
                    value="<?php echo esc_attr(get_user_meta($user->ID, 'user_code_postal', true)); ?>"
                    name="user_code_postal"
                    id="user_code_postal"
                    class="regular-text"
                >
            </td>
        </tr>
        <tr>
            <th><label for="user_city">Ville</label></th>
            <td>
                <input
                    type="text"
                    value="<?php echo esc_attr(get_user_meta($user->ID, 'user_city', true)); ?>"
                    name="user_city"
                    id="user_city"
                    class="regular-text"
                >
            </td>
        </tr>
        <tr>
            <th><label for="user_departement">Département</label></th>
            <td>
                <select name="user_departement">
                    <?php
                    $tab_departements = arrayDepartements();
                    foreach ($tab_departements as $key => $value):?>
                        <option value="<?php echo $key;?>" <?php selected( $key, get_the_author_meta( 'user_departement', $user->ID ) ); ?>><?php echo $key;?> - <?php echo $value;?> </option>
                    <?php endforeach;?>
                </select>
            </td>
        </tr>
        <tr>
            <th><label for="user_region">Région</label></th>
            <td>
                <select name="user_region">
                    <?php
                    $arrayRegions = arrayRegions();
                    foreach ($arrayRegions as $key => $value):?>
                        <option value="<?php echo $key;?>" <?php selected( $key, get_the_author_meta( 'user_region', $user->ID ) ); ?>><?php echo $value;?> </option>
                    <?php endforeach;?>
                </select>
            </td>
        </tr>

        <tr>
            <th><label for="type_structure">Type de structure</label></th>
            <td>
                <?php
                $whichStructureChecked = array();
                $whichStructureChecked["citizen"] = "Citoyen";
                $whichStructureChecked["association"] = "Association";
                $whichStructureChecked["school"] = "École";
                $whichStructureChecked["collectivity"] = "Collectivité";
                $whichStructureChecked["company"] = "Entreprise";
                ?>
                <select name="type_structure">
                <option value="">Filtrer par Type de structure</option>
                <?php
                    $current_v = isset($_GET['type_structure'])? $_GET['type_structure']:'';
                    foreach ($whichStructureChecked as $label => $value) {?>
                        <option value="<?php echo $label;?>" <?php selected( $label, get_the_author_meta( 'type_structure', $user->ID ) ); ?>><?php echo $value;?> </option>
                    <?php }
                ?>
                </select>
            </td>
        </tr>
        <tr>
            <th><label for="structure_name">Nom de la structure</label></th>
            <td>
                <input
                    type="text"
                    value="<?php echo esc_attr(get_user_meta($user->ID, 'structure_name', true)); ?>"
                    name="structure_name"
                    id="structure_name"
                    class="regular-text"
                >
            </td>
        </tr>
        <tr>
            <th><label for="user_district">Quartier</label></th>
            <td>
                <input
                    type="text"
                    value="<?php echo esc_attr(get_user_meta($user->ID, 'user_district', true)); ?>"
                    name="user_district"
                    id="user_district"
                    class="regular-text"
                >
            </td>
        </tr>
      
        

    </table>
<?php
    } 
}

add_action( 'personal_options_update', 'update_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'update_extra_profile_fields' );
function update_extra_profile_fields( $user_id ) {
    if ( current_user_can( 'edit_user', $user_id ) ){
        update_user_meta( $user_id, 'level_ambassadeur', $_POST['level_ambassadeur'] );
        update_user_meta( $user_id, 'user_adress', $_POST['user_adress'] );
        update_user_meta( $user_id, 'user_code_postal', $_POST['user_code_postal'] );
        update_user_meta( $user_id, 'user_departement', $_POST['user_departement'] );
        update_user_meta( $user_id, 'user_phone', $_POST['user_phone'] );
        update_user_meta( $user_id, 'user_city', $_POST['user_city'] );
        update_user_meta( $user_id, 'user_region', $_POST['user_region'] );
        update_user_meta( $user_id, 'user_gender', $_POST['user_gender'] );
        update_user_meta( $user_id, 'type_structure', $_POST['type_structure'] );
        update_user_meta( $user_id, 'structure_name', $_POST['structure_name'] );
        update_user_meta( $user_id, 'user_district', $_POST['user_district'] );
        update_user_meta( $user_id, 'number_authorisation', $_POST['number_authorisation'] );
    }
}

add_action( 'set_user_role', 'ihag_user_role_update', 10,3);
function ihag_user_role_update( $user_id, $role, $old_roles ) 
{
    if(in_array( 'ambassadeur_to_validate', $old_roles, true )  && $role == "ambassadeur"){
        $user = get_userdata($user_id);
        $headers[] = 'From: '.get_bloginfo('name').' <'. __('no-reply@', 'cwcud') . str_replace('www.', '', $_SERVER['SERVER_NAME'] ) .'>';
        $to = $user->user_email;
        $subject = "Votre candidature d'Ambassadeur du Cyber World CleanUp Day est validée";
        $body = "Bonjour ".$user->first_name." ".$user->last_name."<br><br>
Nous avons le plaisir de vous compter parmi nos ambassadeurs pour cette édition du Cyber World CleanUp Day.<br>
Vos coordonnées sont désormais accessibles sur l'annuaire des ambassadeurs.<br>
Nous vous invitons à consulter les outils Ambassadeurs.<br><br>

Numériquement Vôtre,<br>
l’équipe du Cyber World CleanUp Day<br><br>

Vous recevez ce mail car vous prenez part au Cyber World CleanUp Day 2021 et en avez accepté les conditions. Vous pouvez à tout moment exercer votre droit d'accès et/ou de suppression des données vous concernant en nous contactant par email : contact@cyberworldcleanupday.fr";
        wp_mail( $to, $subject, $body, $headers);

        require dirname(__DIR__, 1).'/vendor/autoload.php';

        $mj = new \Mailjet\Client(get_field('id_mailjet', 'option'), get_field('mdp_mailjet', 'option'),true,['version' => 'v3']);
        $body = [
        'Action' => "addnoforce",
        'Contacts' => [
                [
                'Email' => $user->user_email,
                'IsExcludedFromCampaigns' => "false",
                'Name' => $user->user_email,
                'Properties' => "object"
                ]
            ]
        ];
        $response = $mj->post(Resources::$ContactslistManagemanycontacts, ['id' => get_field('id_liste_ambassadeur', 'option'), 'body' => $body]);
        $response->success() && var_dump($response->getData());

   }

}

/**
 * Ne pas afficher l’admin bar pour les simples Ambassadeurs
 *
 * @see https://codex.wordpress.org/Function_Reference/show_admin_bar
 */
add_filter( 'show_admin_bar', 'w_demo_show_admin_bar' );
function w_demo_show_admin_bar( $show ) {
	if ( current_user_can( 'ambassadeur' ) ) {
		$show = false;
	}
	return $show;
}

/*
* traitement du post du form-ambassadeur
*Créé une route pour enregistrer au bon endroit
*/
add_action('rest_api_init', function() {
   
        register_rest_route( 'ihag', 'ambassadeurForm',
            array(
                'methods' 				=> 'POST', //WP_REST_Server::READABLE,
                'callback'        		=> 'ihag_ambassadeurForm',
                'permission_callback' 	=> array(),
                'args' 					=> array(),
            )
        );
    
});

/* Fonction callback de ambassadeurForm */
/**
 * Créer un nouvel utilisateur
 *
 * @see https://developer.wordpress.org/reference/hooks/admin_post_nopriv_action/
 * @see https://codex.wordpress.org/Function_Reference/check_admin_referer
 * @see https://codex.wordpress.org/Validating_Sanitizing_and_Escaping_User_Data
 * @see https://codex.wordpress.org/Function_Reference/wp_insert_user
 * @see https://codex.wordpress.org/Function_Reference/wp_signon
 */
function ihag_ambassadeurForm(WP_REST_Request $request){

    if(isset($_POST['honeyPot']) && empty($_POST['honeyPot'])){
        //Génerer un PWD
        $pwd = wp_generate_password();

        // Création de l’utilisateur
        $infos = array(
            'first_name'   => sanitize_text_field( $_POST['user_firstname'] ),
            'last_name'    => sanitize_text_field( $_POST['user_lastname'] ),
            'user_login'   => sanitize_email($_POST['user_email']),
            'user_email'   => sanitize_email($_POST['user_email']),
            'user_pass'    => $pwd,
            'role'         => 'ambassadeur_to_validate',
            'display_name' => sanitize_text_field( $_POST['user_firstname'] ).' '.sanitize_text_field( $_POST['user_lastname'] ) ,
        );
        $id = wp_insert_user( $infos );

        if ( is_wp_error( $id ) ) { 
            //echo $id->get_error_message();
        
            $user = get_user_by( 'email', sanitize_email($_POST['user_email']) );
            if ( $user ) {//user existe déjà, on lui affecte le role ambassadeur
                $user_id = $user->ID;
                $user_roles = $user->roles;
                // Check if the role you're interested not in
                if ( !in_array( 'ambassadeur_to_validate', $user_roles, true ) ) {
                    $user->add_role( 'ambassadeur_to_validate' );
                }
            } else {
                return new WP_REST_Response( '', 304 );
            }
        }

        // add user meta
        add_user_meta($id, 'user_gender', sanitize_text_field( $_POST['user_gender'] ));
        add_user_meta($id, 'type_structure', sanitize_text_field( $_POST['type_structure'] ));
        if (isset($_POST['structure_name'])){
            add_user_meta($id, 'structure_name', sanitize_text_field( $_POST['structure_name'] ));
        }
        //add_user_meta($id, 'level_ambassadeur', sanitize_text_field( $_POST['level_ambassadeur'] ));
        add_user_meta($id, 'user_city', sanitize_text_field( $_POST['user_city'] ));
        add_user_meta($id, 'user_departement', sanitize_text_field( $_POST['dep'] ));
        add_user_meta($id, 'user_region', sanitize_text_field( $_POST['reg'] ));
        if (isset($_POST['user_district'])){
            add_user_meta($id, 'user_district', sanitize_text_field( $_POST['user_district'] ));
        }
        add_user_meta($id, 'user_code_postal', sanitize_text_field( $_POST['user_code_postal'] ));
        add_user_meta($id, 'user_adress', sanitize_text_field( $_POST['user_adress'] ));
        add_user_meta($id, 'user_phone', sanitize_text_field( $_POST['user_phone'] ));
        add_user_meta($id, 'number_authorisation', sanitize_text_field( $_POST['number_authorisation'] ));


        $headers[] = 'From: '.get_bloginfo('name').' <'. __('no-reply@', 'cwcud') . str_replace('www.', '', $_SERVER['SERVER_NAME'] ) .'>';
        $to = sanitize_email($_POST['user_email']);
        $subject = "Votre candidature d'Ambassadeur du Cyber World CleanUp Day est en attente de validation";
        $body = "Bonjour ".$_POST['user_firstname']." ".$_POST['user_lastname']."<br><br>
Vous venez de vous inscrire en tant qu'Ambassadeur pour le Cyber World CleanUp Day France.<br>
L'ambassadeur est le porte-parole du mouvement à l'échelle locale. Dès lors, il est important que vous ayez toutes les clés en main pour mener à bien votre mission. Pour cela, quoi de mieux que d'échanger de vive voix ?<br><br>
Les coordonnées communiquées sont :<br>
● Mail : ".sanitize_email($_POST['user_email'])."<br>
● Tel : ".sanitize_text_field( $_POST['user_phone'] )."<br><br>
Si ce n’est pas déjà pas fait, nous vous invitons à suivre le MOOC de sensibilisation NR :<br>
● https://www.academie-nr.org/sensibilisation/#/<br><br>
Nous ne manquerons pas de vous contacter dans les plus brefs délais afin de valider avec vous ce rôle si gratifiant et porteur d'énergie positive.<br><br>

Numériquement Vôtre,<br>
l’équipe du Cyber World CleanUp Day<br><br>

Vous recevez ce mail car vous prenez part au Cyber World CleanUp Day 2021 et en avez accepté les conditions. Vous pouvez à tout moment exercer votre droit d'accès et/ou de suppression des données vous concernant en nous contactant par email : contact@cyberworldcleanupday.fr";
        wp_mail( $to, $subject, $body, $headers);
        return new WP_REST_Response( '', 200 );
    }
    return new WP_REST_Response( '', 304 );
}




function ihagCoordonateAmbassadeur(){
	if ( check_nonce() ) {
		$r = '';
		$user_infos = get_userdata(sanitize_text_field($_POST['ambasssadeur_id']));?>
        <p style="text-align:center;cursor:pointer;color:white" class="close-modale" onclick="document.getElementById('modal_ambassadeur').classList.remove('active')">X Fermer</p>
		<div class="embassy-modale">
			<!-- name -->
			<p><strong><?php 
				$gender = get_user_meta($_POST['ambasssadeur_id'], 'user_gender', true);
				if ($gender === 'user_male'):
					echo _e('Mr', 'cwcud').' '.$user_infos->display_name;
				elseif ($gender === 'user_female'):
					echo _e('Mme', 'cwcud').' '.$user_infos->display_name;
				endif;
			?></strong></p>

			<!-- structure  -->
			<p>
				<strong><?php
				$nameStructure = get_user_meta($_POST['ambasssadeur_id'], 'structure_name', true);
				if(!empty($nameStructure)):
					echo $nameStructure;
				endif;
				?></strong>
			</p>

			<!-- Level ambassadeur + département -->
			<p>
			<?php 
			$departement = arrayDepartements();
			$region = arrayRegions();
			$nameReg = $region[get_user_meta($_POST['ambasssadeur_id'], 'user_region', true )];
			$nameDep = $departement[get_user_meta($_POST['ambasssadeur_id'], 'user_departement', true )];
			$nameCity = get_user_meta($_POST['ambasssadeur_id'], 'user_city', true );

			if (get_user_meta($_POST['ambasssadeur_id'], 'level_ambassadeur', true ) === 'regional'):
				echo 'Ambassadeur régional : '.$nameReg;
			elseif (get_user_meta($_POST['ambasssadeur_id'], 'level_ambassadeur', true ) === 'departemental'):
				echo 'Ambassadeur départemental : '.$nameDep;
            elseif (get_user_meta($_POST['ambasssadeur_id'], 'level_ambassadeur', true ) === 'national'):
                echo 'Ambassadeur national';
			elseif (get_user_meta($_POST['ambasssadeur_id'], 'level_ambassadeur', true ) === 'local'):
				echo 'Ambassadeur local : '.$nameCity;
			endif;
			?>
			</p>

			<!-- Mail -->
			<p>
				<?php _e('Adresse e-mail :', 'cwcud');
				$mail = $user_infos->user_email?>
				<a href="mailto:<?php echo $mail; ?>" class="link-underlined"><?php echo $mail; ?></a>
			</p>

			<!-- Phone --> 
			<p>
				<?php 
					if (get_user_meta($_POST['ambasssadeur_id'], 'number_authorisation', true ) === 'number_yes'):
						_e('Téléphone : ', 'cwcud');
						echo get_user_meta(sanitize_text_field($_POST['ambasssadeur_id']), 'user_phone', true);
					endif;
				?>		
			</p>

			<button class="button" id="btnCloseModal" onclick="document.getElementById('modal_ambassadeur').classList.remove('active')"><?php _e('Ok, Merci !', 'cwcud'); ?></button>
		
		</div>
		}
		<?php
		return new WP_REST_Response( $r, 200 );
	}
}

// Schedule an action if it's not already scheduled
if ( ! wp_next_scheduled( 'ihag_notification_new_ambassadeur' ) ) {
    wp_schedule_event( time(), 'daily', 'ihag_notification_new_ambassadeur' );
}

// 1 fois par heure, les orders qui sont en process avec un ped passe en status completed
add_action( 'ihag_notification_new_ambassadeur', 'ihag_notification_new_ambassadeur_send' );
function ihag_notification_new_ambassadeur_send() {
	
    $body = "Candidature d'Ambassadeur en attente de validation<br><br>";
    $users = get_users('role=ambassadeur_to_validate');
    if($users){
        foreach ( $users as $user ) :
            $body .= '● <a href="'.admin_url( 'user-edit.php?user_id=' . $user->ID ).'">'.$user->first_name.' '.$user->last_name. '</a> (mail : <a href="mailto:'.$user->user_email.'"style="color: #888">'.$user->user_email.'</a> - tel : '.$user->user_phone.')'."<br>";
        endforeach;
        $headers = array('Content-Type: text; charset=UTF-8','From: '.get_bloginfo('name').' <'. __('no-reply@', 'cwcud') . str_replace('www.', '', $_SERVER['SERVER_NAME'] ) .'');
        $subject = "Candidature d'Ambassadeur en attente de validation";
        $to = get_option('admin_email');
        wp_mail( $to, 'Ambassadeur en attente de validation', $body, $headers);
    }
}
