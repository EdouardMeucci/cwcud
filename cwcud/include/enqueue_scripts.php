<?php
 
 
add_action('wp_enqueue_scripts', 'site_scripts', 999);
function site_scripts()
{
    global $wp_styles;
    wp_enqueue_style('styles', get_template_directory_uri() . '/style.css');
    wp_deregister_script('jquery');

    wp_dequeue_style( 'wp-block-library' );

    wp_register_script('script', get_stylesheet_directory_uri() . '/script.js', false, false, 'all');
    wp_enqueue_script('script');
    wp_localize_script('script', 'resturl', get_bloginfo('url') . '/wp-json/ihag/');
    wp_localize_script( 'script', 'wpApiSettings', array(
        'root' => esc_url_raw( rest_url() ),
        'nonce' => wp_create_nonce( 'wp_rest' )
    ) );
    wp_localize_script('script', 'home_url', home_url());
}


function remove_field_user_css()
{
    echo '<style>
    #profile-page tr.user-url-wrap,
    #profile-page tr.user-facebook-wrap,
    #profile-page tr.user-instagram-wrap,
    #profile-page tr.user-linkedin-wrap,
    #profile-page tr.user-myspace-wrap,
    #profile-page tr.user-pinterest-wrap,
    #profile-page tr.user-soundcloud-wrap,
    #profile-page tr.user-tumblr-wrap,
    #profile-page tr.user-youtube-wrap,
    #profile-page tr.user-wikipedia-wrap,
    #profile-page tr.user-twitter-wrap,
    #profile-page tr.user-description-wrap,
    #profile-page tr.user-profile-wrap,
    #profile-page div.yoast-settings,
    #profile-page .user-rich-editing-wrap,
    #profile-page .user-syntax-highlighting-wrap,
    #profile-page .user-admin-color-wrap,
    #profile-page .user-comment-shortcuts-wrap,
    #profile-page .user-admin-bar-front-wrap
    {
        display: none;
    }
    
    </style>';
}
add_action( 'admin_head-user-edit.php', 'remove_field_user_css' );
add_action( 'admin_head-profile.php',   'remove_field_user_css' );
add_filter( 'ure_show_additional_capabilities_section', '__return_false' );
