<?php
//pour la newsletter
use \Mailjet\Resources;


/*
* traitement du post du form de Contact
* enregistrement des values dans le custom post type
*/
add_action('rest_api_init', function() {
	register_rest_route( 'ihag', 'newsletterForm',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'ihagFormNewsletter',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);

	register_rest_route( 'ihag', 'mailjetForm',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'ihagFormMailjet',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);

	
});

function ihagFormNewsletter(WP_REST_Request $request){
	
	if (empty($_POST['honeypot'])) {
				
		//Newsletter
		if(!empty($_POST['email_newsletter'])):
			require dirname(__DIR__, 1).'/vendor/autoload.php';

			$mj = new \Mailjet\Client(get_field('id_mailjet', 'option'), get_field('mdp_mailjet', 'option'),true,['version' => 'v3']);
			$body = [
			'Action' => "addnoforce",
			'Contacts' => [
					[
					'Email' => sanitize_text_field($_POST['email_newsletter']),
					'IsExcludedFromCampaigns' => "false",
					'Name' => sanitize_text_field($_POST['email_newsletter']),
					'Properties' => "object"
					]
				]
			];
			$response = $mj->post(Resources::$ContactslistManagemanycontacts, ['id' => get_field('id_liste', 'option'), 'body' => $body]);
			$response->success() && var_dump($response->getData());
			return new WP_REST_Response( '', 200 );

		endif;
	}		
	
	return new WP_REST_Response( '', 304 );
}

function ihagFormMailjet(WP_REST_Request $request){
	
	if (empty($_POST['honeypot'])) {
				
		//Newsletter
		if(!empty($_POST['email_newsletter'])):
			require dirname(__DIR__, 1).'/vendor/autoload.php';

			$mj = new \Mailjet\Client(get_field('id_mailjet', 'option'), get_field('mdp_mailjet', 'option'),true,['version' => 'v3']);
			$body = [
			'Action' => "addnoforce",
			'Contacts' => [
					[
					'Email' => sanitize_text_field($_POST['email_newsletter']),
					'IsExcludedFromCampaigns' => "false",
					'Name' => sanitize_text_field($_POST['email_newsletter']),
					'Properties' => "object"
					]
				]
			];
			$response = $mj->post(Resources::$ContactslistManagemanycontacts, ['id' => get_field('id_liste_pre_inscription', 'option'), 'body' => $body]);
			$response->success() && var_dump($response->getData());
			return new WP_REST_Response( '', 200 );

		endif;
	}		
	
	return new WP_REST_Response( '', 304 );
}



function check_nonce(){
	global $wp_rest_auth_cookie;
	/*
	 * Is cookie authentication being used? (If we get an auth
	 * error, but we're still logged in, another authentication
	 * must have been used.)
	 */
	if ( true !== $wp_rest_auth_cookie && is_user_logged_in() ) {
		return false;
	}
	// Is there a nonce?
	$nonce = null;
	if ( isset( $_REQUEST['_wp_rest_nonce'] ) ) {
		$nonce = $_REQUEST['_wp_rest_nonce'];
	} elseif ( isset( $_SERVER['HTTP_X_WP_NONCE'] ) ) {
		$nonce = $_SERVER['HTTP_X_WP_NONCE'];
	}
	if ( null === $nonce ) {
		// No nonce at all, so act as if it's an unauthenticated request.
		wp_set_current_user( 0 );
		return false;
	}
	// Check the nonce.
	return wp_verify_nonce( $nonce, 'wp_rest' );
}
