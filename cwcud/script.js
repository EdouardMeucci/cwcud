if (document.forms.namedItem("contactForm")) {
    document.forms.namedItem("contactForm").addEventListener('submit', function(e) {
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("contactForm");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'contactForm', true);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessage').disabled = false;
                document.getElementById('sendMessage').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessage').classList.add("showResponseMessage");
            }
        };
        xhr.send(formData);
    });
}



// if (document.forms.namedItem("footer-newslettter-form")) {
//     document.forms.namedItem("footer-newslettter-form").addEventListener('submit', function(e) {
//         document.getElementById('sendMessageNewsletter').disabled = true; //limit le problème des doubles clic
//         e.preventDefault();

//         var form = document.forms.namedItem("footer-newslettter-form");
//         var formData = new FormData(form);
//         xhr = new XMLHttpRequest();
//         xhr.open('POST', resturl + 'formNewsletter', true);
//         xhr.onload = function() {
//             if (xhr.status === 200) {
//                 document.getElementById('sendMessageNewsletter').disabled = false;
//                 document.getElementById('sendMessageNewsletter').classList.add("hiddenSubmitButton");
//                 document.getElementById('ResponseMessageNewsletter').innerText = xhr.response.replace('"', '').replace('"', '');
//                 document.getElementById('ResponseMessageNewsletter').classList.add("showResponseMessage");
//             }
//         };
//         xhr.send(formData);
//     });
// }
if (document.getElementById("ambassadeurForm")) {
    //submit form
    document.forms.namedItem("ambassadeurForm").addEventListener('submit', function(e) {
        document.getElementById('sendMessageAmbassadeur').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("ambassadeurForm");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'ambassadeurForm', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessageAmbassadeur').disabled = false;
                document.getElementById('sendMessageAmbassadeur').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessageAmbassadeur').classList.add("showResponseMessage");
            }
            if (xhr.status === 304) {
                location.href = home_url;
            }
        };
        xhr.send(formData);
    });
}

function displayRadioValue() {
    var ele = document.getElementsByClassName('structure');
    for (i = 0; i < ele.length; i++) {
        if ((ele[i].id == 'citizen') && (ele[i].checked)) {
            document.getElementById("structure_name").style.display = 'none';
        } else if (ele[i].id == 'citizen') {
            document.getElementById("structure_name").style.display = 'inline-block';
        }
    }
}
if (document.getElementById("newsletterForm")) {
    //submit form
    document.forms.namedItem("newsletterForm").addEventListener('submit', function(e) {
        document.getElementById('sendEmail').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("newsletterForm");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'newsletterForm', true);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendEmail').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessage').classList.add("showResponseMessage");
                console.log(xhr.response);
            }
            if (xhr.status === 304) {
                console.log("You're a robot");
            }
        };
        xhr.send(formData);
    });
}

if (document.getElementById("mailjetForm")) {
    //submit form
    document.forms.namedItem("mailjetForm").addEventListener('submit', function(e) {
        document.getElementById('sendEmailMailjet').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("mailjetForm");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'mailjetForm', true);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendEmailMailjet').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessageMailjet').classList.add("showResponseMessage");
                console.log(xhr.response);
            }
            if (xhr.status === 304) {
                console.log("You're a robot");
            }
        };
        xhr.send(formData);
    });
}
/*
Animate header.php (see header.scss)
*/

// Open & Close menu (for mobile devices)
function toggleMenu() {
    document.getElementById("menu").classList.toggle("menu-open");
}

document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("btn-modale");
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            e.preventDefault();
            e.stopPropagation();

            var modale = document.createElement("div");
            modale.classList.add("modale");
            modale.id = el.dataset.uniqId;

            var button = document.createElement("button");
            button.classList.add("closeModale");
            button.onclick = function() { document.getElementById(el.dataset.uniqId).remove(); };
            button.appendChild(document.createTextNode("X"));
            modale.appendChild(button);

            var modaleContent = document.createElement("div");
            modaleContent.id = 'modaleContent' + el.dataset.uniqId;
            modaleContent.classList.add("modaleContent");
            modale.appendChild(modaleContent);

            document.body.appendChild(modale);

            for (var key in iframe) {
                // check if the property/key is defined in the object itself, not in parent
                if (iframe.hasOwnProperty(key)) {
                    if (key == el.dataset.uniqId) {
                        document.getElementById('modaleContent' + el.dataset.uniqId).innerHTML = iframe[key];
                        modale.classList.add("active");
                    }
                }
            }
        });
    });
});
/*

*/

document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("get_modal_ambassadeur");
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            e.stopPropagation();
            e.preventDefault();
            var formData = new FormData();
            formData.append('ambasssadeur_id', el.dataset.id);
            xhr = new XMLHttpRequest(); //prepare la requete
            xhr.open('POST', resturl + 'coordonate-ambassadeur', true);
            xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
            xhr.onload = function() {
                if (xhr.status === 200) {
                    document.getElementById('modal_ambassadeur').innerHTML = xhr.response;
                    document.getElementById('modal_ambassadeur').classList.add("active");
                    var els = document.getElementsByClassName("close-modale");
                    Array.prototype.forEach.call(els, function(el) {
                        el.addEventListener("click", function(e) {
                            e.stopPropagation();
                            var els = document.getElementsByClassName("modale");
                            Array.prototype.forEach.call(els, function(el) {
                                el.classList.remove("active");
                            });
                        });
                    });
                }
            };
            xhr.send(formData);

        });
    });
});

var els = document.getElementsByClassName("close-modale");
Array.prototype.forEach.call(els, function(el) {
    el.addEventListener("click", function(e) {
        e.stopPropagation();
        var els = document.getElementsByClassName("modale");
        Array.prototype.forEach.call(els, function(el) {
            el.classList.remove("active");
        });
    });
});
// organise - infinit scroll 
if (document.querySelector('#infinite-list')) {
    var base_url = window.location.href;
    var elm = document.querySelector('#infinite-list');
    // loader = document.querySelector('#loaderPost');
    var page = elm.dataset.page;
    var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    load = false;
    ticking = false;
    window.addEventListener('scroll', function(e) {
        if (!ticking) {
            window.setTimeout(function() {
                ticking = false;
                //Do something
                if (!load && (elm.offsetTop + elm.clientHeight) < (window.scrollY + height)) { //scroll > bas de infinite-list
                    //inserer les éléments suivants
                    if (page >= elm.dataset.nbPageMax) {
                        console.log(load);
                    } else {
                        load = true;
                        readMorePost();
                    }

                }
            }, 300); //fréquence du scroll
        }
        ticking = true;
    });
}



function readMorePost() {
    page++;
    // loader.classList.add("active");
    var formData = new FormData();
    formData.append("page", page);
    formData.append("cpt", elm.dataset.cpt);
    formData.append("taxo", elm.dataset.taxo);
    formData.append("taxo_tag_var", elm.dataset.taxo_tag);

    console.log("readMorePost");

    xhr = new XMLHttpRequest();
    xhr.open('POST', resturl + 'scroll', true);
    xhr.onload = function() {
        if (xhr.status === 200) {
            load = false;
            console.log(xhr.response);
            elm.insertAdjacentHTML('beforeend', xhr.response);
            // window.history.replaceState("", "", elm.dataset.url + "page/" + page + "/");
            // loader.classList.remove("active");
        }
    };
    xhr.send(formData);
}
document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("JSrslink");
    var heightScreen = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    var widthScreen = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            e.stopPropagation();
            e.preventDefault();
            var width = 320,
                height = 400,
                left = (widthScreen - width) / 2,
                top = (heightScreen - height) / 2,
                url = this.href,
                opts = 'status=1' +
                ',width=' + width +
                ',height=' + height +
                ',top=' + top +
                ',left=' + left;
            window.open(url, 'myWindow', opts);
            return false;
        });
    });

    copyLink = document.getElementsByClassName('copyLink');
    Array.prototype.forEach.call(copyLink, function(el) {
        el.addEventListener("click", function(e) {
            e.stopPropagation();
            copyLinkValue = el.getElementsByClassName('copyLinkValue');
            copyLinkValue[0].focus();
            copyLinkValue[0].select();
            document.execCommand("copy");
            el.classList.add('active');
        });

    });



});