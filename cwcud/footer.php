</div><!-- div #content -->

<footer id="footer" class="block-pad h-pad-reg">

	<!-- Footer Left column : RS + Menu + Copyright-->
	<div id="footer-left">

		<!-- réseaux sociaux -->
		<nav id="footer-social">

			<p class="bold"><?php _e('Suivez-nous : ','cwcud'); ?></p>

			<?php 
			// Linkedin
			$link = get_field('linkedin', 'option');
			if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a class="link-icon" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
					<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/linkedin.png" height="40" width="40">
				</a>
			<?php endif; ?>

			<?php 
			// Facebook
			$link = get_field('facebook', 'option');
			if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a class="link-icon" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
					<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/facebook.png
					" height="40" width="40">
				</a>
			<?php endif; ?>

			<?php 
			// Twitter
			$link = get_field('twitter', 'option');
			if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a class="link-icon" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
					<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/twitter.png" height="40" width="40">
				</a>
			<?php endif; 
			?>
			
			<?php 
			// Instagram
			$link = get_field('instagram', 'option');
			if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a class="link-icon" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
					<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/instagram.svg" height="40" width="40">
				</a>
			<?php endif; ?>

			<?php 
			// Youtube
			$link = get_field('youtube', 'option');
			if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a class="link-icon" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
					<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/youtube.png" height="40" width="40">
				</a>
			<?php endif; ?>

		</nav>

		<!-- Menu -->
		<?php echo ihag_menu('secondary'); ?>

		<!-- Copyright -->
		<p id="copyright">© <?php echo date("Y"); ?> <?php _e('Cyber World CleanUp Day FRANCE', 'cwcud');?></p>

		<!-- Custom Content -->
		<?php 
		$text_footer = get_field('content', 'option');
		if ($text_footer) { 
			echo '<div id="footer-content">'. $text_footer.'</div>';
		}

		//Si l'utilisateur n'est pas conecté : Formulaire
		if(!is_user_logged_in()):?>

			<a href="<?php the_permalink(get_field('page_my_account', 'options')); ?>"><?php _e('ESPACE ORGANISATEUR')?></a>
		
		<?php endif;?>


	</div> <!-- /Footer Left column -->

		

	<!-- Footer Right column : Newsletter-->
	<div id="footer-right">
		<p class="big"><?php _e('Recevez toutes les news et les conseils du Cyber World CleanUp Day :', 'cwcud') ?></p>

		<form action="" method="post" name="newsletterForm" id="newsletterForm">
			<label for="email_newsletter"><?php _e('Adresse e-mail', 'cwcud') ?></label>
			<input type="email" name="email_newsletter" id="email_newsletter" required placeholder="<?php _e('adresse@mail.com', 'cwcud'); ?>">

			<!-- ok cgu -->
			<div class="checkbox form-item">
				<input type="checkbox" name="ok_cgu" id="ok_cgu" required >
				<div class="checkbox-label">
					<label for="ok_cgu"><?php _e('J\'accepte le traitement de mes données personnelles.', 'cwcud');?></label>
					<div class="form-info">
						<a class="link-discrete" href="<?php echo get_privacy_policy_url();?>"><?php _e('En savoir plus sur la gestion de vos données et vos droits. *', 'cwcud') ;?></a>
					</div>
				</div>
			</div>

			<p class="form-sub-item no-margin form-legal-text">
				<?php _e('Votre adresse de messagerie est uniquement utilisée pour vous envoyer la newsletter du Cyber World CleanUp Day France. Vous pouvez à tout moment utiliser le lien de désabonnement intégré dans chaque newsletter.', 'cwcud');?>
			</p>

			<input type="hidden" name="honeypot" value="">
			<input type="submit" class="button form-item" id="sendEmail" value="<?php _e('S\'inscrire', 'cwcud'); ?>">
			<div id="ResponseMessage" class="ResponseMessage">
				Merci, votre inscription a été enregistrée.
			</div>
		</form>
	
	</div>
	<!-- /Footer Right column -->
		
</footer><!-- End of #footer -->

<?php wp_footer(); ?>

</body>
</html>