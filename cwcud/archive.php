<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

get_header();
?>

<!-- recup name cpt -->
<?php $cpt = substr(get_queried_object()->taxonomy, 5); ?>

<!-- Header -->
<header class="page-title">
	<?php //wpBreadcrumb(); ?>
	<h1 class="center">
		<?php echo get_the_title(get_field('archive_'.$cpt , 'option'));?> : <?php echo single_cat_title( '', false );?>
	</h1>
</header>

<div class="center narrow-wrapper archive-info">
	<?php echo category_description(); ?>
</div>

<!-- Filters -->
<?php if ( have_posts() ) : ?>
	<div class="wrapper center">
		<?php 
		set_query_var( 'cpt', $cpt );
		get_template_part( 'template-parts/part','taxo' ); 
		?>
	</div>
<?php endif; ?>

<!-- pour le scroll -->
<?php $num_page = (get_query_var("paged") ? get_query_var("paged") : 1);?>

<!-- Listing Archive -->
<section>

	<!-- wrapper -->
	<div class="listing-archive wrapper v-padding-small"

		data-cpt=<?php echo $cpt ; ?>
		data-page="<?php echo $num_page;?>"
		data-nb-page-max="<?php echo ceil(($wp_query->found_posts)/(get_option('posts_per_page' ))); ?>"
		data-url="<?php echo get_category_link(get_queried_object()->term_id);?>" 
		data-taxo="<?php echo get_queried_object()->term_id;?>"
		data-taxo_tag="<?php if(isset($_GET['var_taxo_tag'])): echo $_GET['var_taxo_tag']; endif;?>"

		id="infinite-list">

		<?php if ( have_posts() ) : ?>

			<?php //var_dump($wp_query->found_posts);?>
			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();
				get_template_part( 'template-parts/archive', $cpt  );
			endwhile;
			?>

		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>

	</div><!-- /wrapper -->

</section><!-- End of Listing Archive -->

<?php
get_footer();
