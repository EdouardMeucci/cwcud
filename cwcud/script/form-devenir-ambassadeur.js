if (document.getElementById("ambassadeurForm")) {
    //submit form
    document.forms.namedItem("ambassadeurForm").addEventListener('submit', function(e) {
        document.getElementById('sendMessageAmbassadeur').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("ambassadeurForm");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'ambassadeurForm', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessageAmbassadeur').disabled = false;
                document.getElementById('sendMessageAmbassadeur').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessageAmbassadeur').classList.add("showResponseMessage");
            }
            if (xhr.status === 304) {
                location.href = home_url;
            }
        };
        xhr.send(formData);
    });
}

function displayRadioValue() {
    var ele = document.getElementsByClassName('structure');
    for (i = 0; i < ele.length; i++) {
        if ((ele[i].id == 'citizen') && (ele[i].checked)) {
            document.getElementById("structure_name").style.display = 'none';
        } else if (ele[i].id == 'citizen') {
            document.getElementById("structure_name").style.display = 'inline-block';
        }
    }
}