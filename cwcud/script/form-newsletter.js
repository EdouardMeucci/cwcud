if (document.getElementById("newsletterForm")) {
    //submit form
    document.forms.namedItem("newsletterForm").addEventListener('submit', function(e) {
        document.getElementById('sendEmail').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("newsletterForm");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'newsletterForm', true);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendEmail').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessage').classList.add("showResponseMessage");
                console.log(xhr.response);
            }
            if (xhr.status === 304) {
                console.log("You're a robot");
            }
        };
        xhr.send(formData);
    });
}

if (document.getElementById("mailjetForm")) {
    //submit form
    document.forms.namedItem("mailjetForm").addEventListener('submit', function(e) {
        document.getElementById('sendEmailMailjet').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("mailjetForm");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'mailjetForm', true);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendEmailMailjet').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessageMailjet').classList.add("showResponseMessage");
                console.log(xhr.response);
            }
            if (xhr.status === 304) {
                console.log("You're a robot");
            }
        };
        xhr.send(formData);
    });
}