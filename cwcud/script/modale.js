document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("btn-modale");
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            e.preventDefault();
            e.stopPropagation();

            var modale = document.createElement("div");
            modale.classList.add("modale");
            modale.id = el.dataset.uniqId;

            var button = document.createElement("button");
            button.classList.add("closeModale");
            button.onclick = function() { document.getElementById(el.dataset.uniqId).remove(); };
            button.appendChild(document.createTextNode("X"));
            modale.appendChild(button);

            var modaleContent = document.createElement("div");
            modaleContent.id = 'modaleContent' + el.dataset.uniqId;
            modaleContent.classList.add("modaleContent");
            modale.appendChild(modaleContent);

            document.body.appendChild(modale);

            for (var key in iframe) {
                // check if the property/key is defined in the object itself, not in parent
                if (iframe.hasOwnProperty(key)) {
                    if (key == el.dataset.uniqId) {
                        document.getElementById('modaleContent' + el.dataset.uniqId).innerHTML = iframe[key];
                        modale.classList.add("active");
                    }
                }
            }
        });
    });
});
/*

*/

document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("get_modal_ambassadeur");
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            e.stopPropagation();
            e.preventDefault();
            var formData = new FormData();
            formData.append('ambasssadeur_id', el.dataset.id);
            xhr = new XMLHttpRequest(); //prepare la requete
            xhr.open('POST', resturl + 'coordonate-ambassadeur', true);
            xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
            xhr.onload = function() {
                if (xhr.status === 200) {
                    document.getElementById('modal_ambassadeur').innerHTML = xhr.response;
                    document.getElementById('modal_ambassadeur').classList.add("active");
                    var els = document.getElementsByClassName("close-modale");
                    Array.prototype.forEach.call(els, function(el) {
                        el.addEventListener("click", function(e) {
                            e.stopPropagation();
                            var els = document.getElementsByClassName("modale");
                            Array.prototype.forEach.call(els, function(el) {
                                el.classList.remove("active");
                            });
                        });
                    });
                }
            };
            xhr.send(formData);

        });
    });
});

var els = document.getElementsByClassName("close-modale");
Array.prototype.forEach.call(els, function(el) {
    el.addEventListener("click", function(e) {
        e.stopPropagation();
        var els = document.getElementsByClassName("modale");
        Array.prototype.forEach.call(els, function(el) {
            el.classList.remove("active");
        });
    });
});