Activer le thème I Have a Green

//Dans le fichier wp-config.php, ajouter le code pour générer les logs (juste après wp_debug)
// DEBUG PHP
// -----------------------------------------------------------------------------
$dir_logs = dirname(__FILE__).'/_logs/';
if ( !file_exists($dir_logs) ){ mkdir($dir_logs, 0777);}
ini_set('log_errors', 1);
ini_set('display_errors', 0);
date_default_timezone_set("Europe/Paris");
ini_set('error_log', $dir_logs.'debug-'.date('Ymd').'.log');
ini_set('error_reporting', E_ALL ^ E_NOTICE);

$files = scandir($dir_logs, SCANDIR_SORT_DESCENDING);
if ( count($files) > 5 ) :
    for ( $i = 5; $i < count($files); $i++ ) :
        if ( stripos($files[$i], '.') != 0 ) :
            unlink($dir_logs.$files[$i]);
        endif;
    endfor;
endif;


//dans un terminal,
cd nomDuProjet
npm install

//fichier package.json
Modifier la ligne des “scripts” sur le start : mettre le bon chemin ‘localhost/nomDuProjet’

//Lancer les 2 scripts (dans deux terminaux)
npm run watch
npm run start //pas obligatoire, permet de rafraichir automatiquement la page après chaque modification de fichier


