<?php
/*
Template Name: Contact
*/
?>

<?php get_header(); ?>

<header class="page-title">
	<?php wpBreadcrumb(); ?>
	<?php the_title('<h1 class="center">', '</h1>'); ?>
</header>
        
<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<main id="raw-content">
  <?php the_content(); ?>
</main>

<!-- Contact Form -->
<div class="v-padding-regular gray-light-bg">

  <form class="white-bg v-padding-small" name="contactForm" id="contactForm" action="#" method="POST">

    <input type="hidden" name="nonceformContact" value="">
    
    <label for="nameContact"><?php esc_html_e('Nom', 'ihag')?> *</label>
    <input type="text" name="nameContact" id="nameContact" placeholder="<?php esc_html_e('Martin', 'ihag')?>" required value="">

    <label for="firstNameContact"><?php esc_html_e('Prénom', 'ihag')?> *</label>
    <input type="text" name="firstNameContact" id="firstNameContact" placeholder="<?php esc_html_e('Jean', 'ihag')?>" required value="">

    <label for="nameEntreprise"><?php esc_html_e('Entreprise', 'ihag')?></label>
    <input type="text" name="nameEntreprise" id="nameEntreprise" placeholder="<?php esc_html_e('Nom de la société', 'ihag')?>" value="">

    <label for="emailContact"><?php esc_html_e('Adresse mail', 'ihag')?> *</label>
    <input type="email" name="emailContact" id="emailContact" placeholder="<?php esc_html_e('nom.prenom@exemple.com', 'ihag')?>" required value="">

    <label for="tel"><?php esc_html_e('Téléphone', 'ihag')?></label>
    <input type="tel" name="tel" id="tel" placeholder="06 12 34 56 78" value="">
    
    <label for="messageContact"><?php esc_html_e('Message', 'ihag')?></label>  
    <textarea type="text" name="messageContact" id="messageContact" placeholder="<?php esc_html_e('Rédigez votre message', 'ihag')?>" required rows="8" value=""></textarea>
    
    <i class="body-like gray-medium">* <?php esc_html_e('Champs obligatoires', 'ihag')?></i>
    
    <div class="checkbox">
      <input id="rgpdCheckbox" type="checkbox" required name="rgpdCheckbox">
      <label for="rgpdCheckbox">
        <?php esc_html_e('En cochant cette case et en soumettant ce formulaire, j\'accepte que mes données personnelles soient utilisées pour me recontacter dans le cadre de ma demande indiquée dans ce formulaire. (Aucun autre traitement ne sera effectué avec vos informations)', 'ihag')?>
        <a class="link-default" href="<?php echo get_privacy_policy_url();?>">
          <?php esc_html_e('Voir la politique de confidentialité', 'ihag')?>
        </a>
      </label>
    </div>
    
    <div id="ResponseAnchor" class="center">
      <input class="button-cta" type="submit" id="sendMessage" value="Envoyer le message">
      <p id="ResponseMessage">
        <?php esc_html_e( 'Votre message a été envoyé !', 'ihag' ); ?>
      </p>
    </div>

  </form>

</div>



<!-- End of the loop -->
<?php endwhile; endif;?>

<?php get_footer(); ?>
