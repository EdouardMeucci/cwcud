<?php
/*
Template Name: tpl formulaire - devenir ambassadeur
*/
?>

<?php get_header(); ?>
      
<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<main id="raw-content">
	<?php the_content(); ?>
</main>
<form class="form-style" action="" method="post"  name="ambassadeurForm" id="ambassadeurForm">

    <!-- Honey Pot -->
    <input type="hidden" name="honeyPot" value=""> 
	
    <h2 class="ctr no-margin"><?php _e('Devenir Ambassadeur', 'cwcud');?></h2>

    <p class="ctr form-sub-item"><?php _e('Devenez l\'ambassadeur local de votre ville !', 'cwcud');?></p>
  
    <p class="no-margin form-item label-like"><?php _e('Civilité *', 'cwcud');?></p>
    <div class="form-row">
        <div class="checkbox">
            <input type="radio" name="user_gender" class="gender" id="user_female" value="user_female" checked>
            <label class="checkbox-label"  for="user_female"><?php _e('Madame', 'cwcud');?></label>
        </div>
        <div class="checkbox">
            <input type="radio" name="user_gender" class="gender" id="user_male" value="user_male">
            <label class="checkbox-label" for="user_male"><?php _e('Monsieur', 'cwcud');?></label>
        </div>
    </div>

    
    <label for="user_firstname"><?php _e('Prénom *', 'cwcud'); ?></label>
    <input type="text" id="user_firstname" name="user_firstname" placeholder="Véronique" required>

    <label for="user_lastname"><?php _e('Nom *', 'cwcud'); ?></label>
    <input type="text" id="user_lastname" name="user_lastname" placeholder="Dubois" required>

    <div class="form-item">
        <p class="no-margin label-like"><?php _e('Type de structure *', 'cwcud');?></p>
        <div class="checkbox form-sub-item">
            <input type="radio" name="type_structure" id="citizen" class="structure" value="citizen" onclick="displayRadioValue()" checked>
            <label class="checkbox-label" for="citizen"><?php _e('Citoyen', 'cwcud');?></label>
        </div>
        <div class="checkbox form-sub-item">
            <input type="radio" name="type_structure" id="association" class="structure" value="association" onclick="displayRadioValue()">
            <label class="checkbox-label" for="association"><?php _e('Association', 'cwcud');?></label>
        </div>
        <div class="checkbox form-sub-item">
            <input type="radio" name="type_structure" id="school" class="structure" value="school" onclick="displayRadioValue()">
            <label class="checkbox-label" for="school"><?php _e('École', 'cwcud');?></label>
        </div>
        <div class="checkbox form-sub-item">
            <input type="radio" name="type_structure" id="collectivity" class="structure" value="collectivity" onclick="displayRadioValue()">
            <label class="checkbox-label" for="collectivity"><?php _e('Collectivité', 'cwcud');?></label>
        </div>
        <div class="checkbox form-sub-item">
            <input type="radio" name="type_structure" id="company" class="structure" value="company" onclick="displayRadioValue()">
            <label class="checkbox-label" for="company"><?php _e('Entreprise', 'cwcud');?></label>
        </div>
    </div>


    
    <div id="structure_name" style="display: none;">
        <label for="structure_name"><?php _e('Nom de la structure', 'cwcud'); ?></label>
        <input type="text"  name="structure_name" placeholder="Nom de votre structure" >
    </div>

    <!--<div class="form-item">
        <p class="no-margin label-like"><?php _e('Niveau d\'ambassadeur *', 'cwcud');?></p>
        <div class="checkbox form-sub-item">
            <input type="radio" name="level_ambassadeur" id="regional" class="ambassadeur" value="regional" checked>
            <label class="checkbox-label" for="regional"><?php _e('Ambassadeur régional', 'cwcud');?></label>
        </div>
        <div class="checkbox form-sub-item">
            <input type="radio" name="level_ambassadeur" id="departemental" class="ambassadeur" value="departemental">
            <label class="checkbox-label" for="departemental"><?php _e('Ambassadeur départemental', 'cwcud');?></label>
        </div>
        <div class="checkbox form-sub-item">
            <input type="radio" name="level_ambassadeur" id="local" class="ambassadeur" value="local">
            <label class="checkbox-label" for="local"><?php _e('Ambassadeur local', 'cwcud');?></label>
        </div>
    </div>-->

    <label for="user_adress"><?php _e('Adresse postale *', 'cwcud'); ?></label>
    <input type="text" id="user_adress" name="user_adress" placeholder="75 rue Gemberra" required>
    <p class="form-info">
        <?php _e('* Information destinée uniquement aux administrateurs du site. Votre adresse postale ne sera pas publiée sur le site.', 'cwcud'); ?>
    </p>
    

    <label for="user_district"><?php _e('Quartier', 'cwcud'); ?></label>
    <input type="text" id="user_district" name="user_district" placeholder="VIe arrondissement">
    <label for="user_code_postal"><?php _e('Code postal *', 'cwcud'); ?></label>
    <input type="text" id="user_code_postal" name="user_code_postal" placeholder="75006" required>
    <label for="user_city"><?php _e('Ville *', 'cwcud'); ?></label>
    <input type="text" id="user_city" name="user_city" placeholder="Paris" required>

    <label for="dep"><?php _e('Département :', 'cwcud'); ?>*</label>
    <div class="select">
        <select name="dep" name="dep" required>
            <option value="">Selectionner votre département</option>
            <?php
            $tab_departements = arrayDepartements();
            foreach ($tab_departements as $key => $value):?>
                <option value="<?php echo $key;?>"><?php echo $key;?> - <?php echo $value;?> </option>
            <?php endforeach;?>
        </select>
    </div>

    <label for="reg"><?php _e('Région :', 'cwcud'); ?>*</label>
    <div class="select">
        <select name="reg" id="reg" required>
            <option value="">Selectionner votre région</option>
            <?php
            $arrayRegions = arrayRegions();
            foreach ($arrayRegions as $key => $value):?>
                <option value="<?php echo $key;?>"><?php echo $value;?> </option>
            <?php endforeach;?>
        </select>
    </div>
    
    <label for="user_email"><?php _e('Adresse mail *', 'cwcud'); ?></label>
    <input type="email" id="user_email" name="user_email" placeholder="adresse.mail@exemple.com" required>

    <p class="form-info">
        <?php _e('** Votre adresse mail servira d\'identifiant pour votre compte Ambassadeur. Elle sera publiée sur le site et visible dans l\'annuaire des ambassadeurs.', 'cwcud'); ?>
    </p>


    <label for="user_phone"><?php _e('Téléphone *', 'cwcud'); ?></label>
    <input type="tel" id="user_phone" name="user_phone" placeholder="+33 6 01 02 03 04"  required>
    
    <p class="form-item no-margin label-like"><?php _e('Afficher mon numéro de téléphone dans l\'annuaire des ambassadeurs *', 'cwcud');?></p>
    <div class="form-row">
        <div class="checkbox">
            <input type="radio" name="number_authorisation" id="number_yes" class="number" value="number_yes" checked>
            <label class="checkbox-label" for="number_yes"><?php _e('Oui', 'cwcud');?></label>
        </div>
        <div class="checkbox">
            <input type="radio" name="number_authorisation" id="number_no" class="number" value="number_no">
            <label class="checkbox-label" for="number_no"><?php _e('Non', 'cwcud');?></label>
        </div>
    </div>

    <p class="form-item no-margin form-legal-text"><?php _e('* Champs obligatoires', 'cwcud'); ?></p>

    <p class="form-item no-margin label-like"><?php _e('Je certifie que : ', 'cwcud');?></p>
   
    <!-- certifications -->
    <div class="checkbox form-item">

        <input type="checkbox" name="maj-cotisation-inr" id="maj-cotisation-inr">
        <div class="checkbox-label">
            <label for="maj-cotisation-inr">
                <?php _e('Je suis à jour de cotisation à l\'association Institut du Numérique Responsable ou World CleanUp Day (de janvier 2021 à décembre 2021)');?>
                <?php $link_inr = get_field('link_join_inr', 'option');
                if( $link_inr ): 
                    $link_inr_url = $link_inr['url'];
                    $link_inr_title = $link_inr['title'];
                    $link_inr_target = $link_inr['target'] ? $link_inr['target'] : '_self';
                    ?>
                    <a class="no-display-block" href="<?php echo esc_url( $link_inr_url ); ?>" target="<?php echo esc_attr( $link_inr_target ); ?>">
                        (<?php echo esc_html( $link_inr_title ); ?>)
                    </a>
                <?php endif;?>
                *
            </label>
        </div>
    </div>

    <div class="checkbox form-item">

        <input type="checkbox" name="ok_charte" id="ok_charte" required>
        <div class="checkbox-label">
            <label for="ok_charte">
                <?php _e('Je m\'engage à respecter', 'cwcud');?>
                <?php $charte_link = get_field('charte_link', 'option');
                if( $charte_link ): 
                    $charte_link_url = $charte_link['url'];
                    $charte_link_title = $charte_link['title'];
                    $charte_link_target = $charte_link['target'] ? $charte_link['target'] : '_self';
                    ?>
                    <a class="no-display-block" href="<?php echo esc_url( $charte_link_url ); ?>" target="<?php echo esc_attr( $charte_link_target ); ?>">
                        <?php echo esc_html( $charte_link_title ); ?>
                    </a>
                <?php endif;?>
                *
            </label>
        </div>
    </div>

    <!-- ok cgu -->
    <div class="checkbox form-item">
        <input type="checkbox" name="ok_cgu" id="ok_cgu3" required>
        <div class="checkbox-label">
            <label for="ok_cgu3"><?php _e('J\'accepte le traitement de mes données personnelles dans le cadre du Cyber World CleanUp Day.', 'cwcud');?>*</label>
            <div class="form-info"><a class="link-underlined" href="<?php echo get_privacy_policy_url();?>"><?php _e('En savoir plus sur la gestion de vos données et vos droits.', 'cwcud') ;?></a></div>
        </div>
    </div>

    <!-- ok cgu bis -->
    <div class="checkbox form-item">
        <input type="checkbox" name="ok_cgu" id="ok_cgu2" required>
        <div class="checkbox-label">
            <label for="ok_cgu2"><?php _e('J\'accepte que mon adresse e-mail soit affichée publiquement sur le site du Cyber World CleanUp Day afin de faciliter le contact avec des organisateurs de Cyber CleanUps.', 'cwcud');?>*</label>
            <div class="form-info"><a class="link-underlined" href="<?php echo get_privacy_policy_url();?>"><?php _e('En savoir plus sur la gestion de vos données et vos droits.', 'cwcud') ;?></a></div>
        </div>
    </div>

    <!-- Button submit -->
	<button class="button form-item"  type="submit" id="sendMessageAmbassadeur"><?php _e('Envoyer la candidature', 'cwcud'); ?></button>
    <div id="ResponseMessageAmbassadeur" style="text-align:center;padding:2rem;">
        <?php _e('Candidature enregistrée avec succès. Un email de confirmation a été envoyé.', 'cwcud');?>
    </div>
</form>


<!-- End of the loop -->
<?php endwhile; endif;?>

<?php
get_footer();
