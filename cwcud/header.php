<?php
/**
 * The header for our theme
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-16px.png" sizes="16x16">
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-32px.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-96px.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-124px.png" sizes="124x124">
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-512px.png" sizes="512x512">

	<link rel="apple-touch-icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-124px.png" />
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<!-- Skip Links-->
	<a class="skip-link" tabindex="0"  href="#content"><?php esc_html_e( 'Accéder au contenu', 'ihag' ); ?></a>
	<a class="skip-link" tabindex="0" href="#menu"><?php esc_html_e( 'Menu', 'ihag' ); ?></a>
	<a class="skip-link" tabindex="0"  href="#footer"><?php esc_html_e( 'Accéder au pied de page', 'ihag' ); ?></a>

	<?php if(is_user_logged_in()):
		$user = wp_get_current_user();
		$user_roles = $user->roles;
		if ( in_array( 'organisateur', $user_roles, true ) ):
	?>
			<!--if connect : organisateur - topbar  -->
			<nav id="userbar">
				<div>
					<p><?php _e('Connecté en tant que', 'cwcud'); ?> <strong><?php echo $user->display_name ;?></strong></p>
					<ul>
						<li><a class="link-underlined" href="<?php the_permalink(get_field('page_my_account', 'options')); ?>"><?php _e('Mon compte', 'cwcud'); ?></a></li>
						<li><a class="link-underlined" href="<?php the_permalink(get_field('page_list_cleanup', 'options')); ?>"><?php _e('Mes Cyber CleanUps'); ?></a></li>
					</ul>
				</div>
			
				<a href="<?php echo wp_logout_url( home_url() ); ?>" class="button-yellow">			
					<?php _e('Se déconnecter', 'cwcud');?>
				</a>
			</nav>
		<?php endif; ?>
	<?php endif; ?>


	<nav id="topbar" class="h-pad-reg">
		<a id="topbar-logo" href="<?php echo get_home_url(); ?>" alt="<?php esc_html_e( 'Lien vers la page d\'accueil', 'ihag') ?>">
			<?php 
			//Custom Logo
			$logo = get_field('logo', 'options');
			$size = 'logo';
			if( $logo ) { 
				//echo '<img alt="'; esc_html_e('Logo', 'ihag'); echo '" src="'.wp_get_attachment_image_url( $logo, $size ).'">';
				$srcset = wp_get_attachment_image_srcset($logo, 'logo');
				$srcx2 = wp_get_attachment_image_src($logo, 'large');
				$srcset = $srcx2[0].' 2x, '.$srcset;
				echo wp_get_attachment_image($logo, $size, 0, array('srcset'=>$srcset));
			}
			?>
		</a>

		<!-- Burger button -->
		<button id="burger-menu" class="reset-style desktop-hidden huge-hidden" onclick="toggleMenu()" aria-label="<?php esc_html_e( 'Ouvrir le menu', 'ihag' ); ?>">
			<img aria-hidden="true" src="<?php echo get_stylesheet_directory_uri(); ?>/image/burger.svg" height="18" width="24">
		</button>

		<div id="menu">

			<!-- Close Button -->
			<div id="close-menu">
				<button class="reset-style desktop-hidden huge-hidden" onclick="toggleMenu()" aria-label="<?php esc_html_e( 'Fermer le menu', 'ihag' ); ?>">
					<img aria-hidden="true" src="<?php echo get_stylesheet_directory_uri(); ?>/image/cross.svg" height="36" width="36">
				</button>
			</div>
			
			<!-- Menu -->
			<?php echo ihag_menu('second'); ?>
			<?php echo ihag_menu('primary'); ?>

		</div>
	</nav>

	<!-- #content -->
	<div id="content">
