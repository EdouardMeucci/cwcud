<?php
/**
 * Block Name: Titre
 */
 ?>

<?php
$title = get_field('title');
$html = get_field('level_title');

if ( empty($title) ):?>
	<em>Renseigner le titre</em>
<?php else :

	echo "<". $html ." class='custom-title'>". $title ."</". $html .">";

endif; ?>


