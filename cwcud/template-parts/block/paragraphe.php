<?php
/**
 * Block Name: Paragraphe
 */
 ?>

<?php
$texte = get_field('text');

if ( empty($texte) ):?>

	<em>Renseigner le paragraphe</em>

<?php else :?>

	<div class="entry-content narrow-wrapper"><?php echo $texte; ?></div>

<?php endif; ?>
