<?php
/**
 * Block Name: Mission
 */
 ?>

<article class="blk-mission blk-wp lft block-pad-top">

<?php

$image = get_field('image');

if ( empty($image) ):
	echo '<em>Renseigner le bloc</em>';
else :
?>

	<div class="mission-card">
		
		<div class="img-content">
			<?php echo wp_get_attachment_image($image, 'medium'); ?>
		</div>

		<div class="txt-content">
			<div class="txt">
				<?php if(!empty(get_field('title'))):?>
					<h2><?php the_field('title');?></h2>
				<?php endif; ?>

				<?php if(!empty(get_field('text'))):?>
					<div><?php the_field('text');?></div>
				<?php endif; ?>
			</div>
		</div>

	</div>
	
<?php endif; ?>

</article>

