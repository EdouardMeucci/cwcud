<?php
/**
 * Block Name: Texte - 3 paragraphes
 */
 ?>


<?php
$title = get_field('title');
$html = get_field('level_title');
?>

<section class="blk-txt blk-wp wrapper block-pad">

	<?php
	if ( empty($html) ):
		echo '<em>Renseigner le titre</em>';
	else :

		// title
		echo '<'. $html .' class="title">'. $title .'</'. $html .'>';?>
		
		<div class="container3P">
			<div>
				<?php if(!empty(get_field('subtitle1'))): ?>
					<span class="h3-like">
						<?php the_field('subtitle1'); ?>
					</span>
				<?php endif;?>

				<?php the_field('text1') ;?>
			</div>

			<div>
				<?php if(!empty(get_field('subtitle2'))): ?>
					<span class="h3-like">
						<?php the_field('subtitle2'); ?>
					</span>
				<?php endif;?>

				<?php the_field('text2') ;?>
			</div>	

			<div>
				<?php if(!empty(get_field('subtitle3'))): ?>
					<span class="h3-like">
						<?php the_field('subtitle3'); ?>
					</span>
				<?php endif;?>

				<?php the_field('text3') ;?>
			</div>
		</div>

	<?php 
	endif; 
	?>

</section>
