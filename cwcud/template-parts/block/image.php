<?php
/**
 * Block Name: Bloc Image
 */
 ?>

<?php 

// Options : 'standard' or 'fullscreen'
$size = get_field('format_size');
$image = get_field('image');

if ( !$image ):

	echo '<em>Renseigner l\'image</em>';
	
else :
?>

<div class="blk-img-<?php echo $size;?> blk-wp lft block-pad">
		
	<?php echo wp_get_attachment_image($image, $size); ?> 
	
</div>

<?php endif; ?>
