<?php
/**
 * Block Name: Évènement - Accueil
 */
 ?>

<section class="blk-event blk-wp ctr">

<?php

$image = get_field('background_image');

if ( empty($image) ):?>
		<em>Renseigner le bloc</em>
<?php else :?>

	<!-- wrapper -->
	<div class="txt-content wrapper v-pad-reg">

		<?php if(!empty(get_field('title'))):?>
			<h2 class="event-title"><?php the_field('title'); ?></h2>
		<?php endif; ?>


		<?php if(!empty(get_field('hook'))):?>
			<p class="event-txt"><?php the_field('hook'); ?></p>
		<?php endif; ?>

		<?php if(!empty(get_field('date'))):?>
			<p class="event-date" style='text-transform: capitalize'><?php the_field('date'); ?></p>
		<?php endif; ?>

		<?php 
		$link = get_field('link');
		if( $link ): 
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self';
			?>
			<a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
		<?php endif; ?>

	</div><!-- /wrapper -->

	<div class="img-content">
		<?php echo wp_get_attachment_image($image, 'event'); ?>
	</div>
	
<?php endif; ?>

</section>

