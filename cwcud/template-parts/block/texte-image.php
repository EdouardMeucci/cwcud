<?php
/**
 * Block Name: Texte & image
 */
 ?>

 <?php
$title = get_field('title');
$html = get_field('level_title');
$text = get_field('text');
$image = get_field('image');

// Image position
$position = get_field('image_position');
?>

<section class="blk-txt-img blk-wp wrapper block-pad img-<?php echo $position ?>">

	<?php
	if ( empty($image) ):
		echo '<em>Renseigner le bloc</em>';
	else :

		// Part 1 : Text
		if ( ($title) || ($text) ):

			echo '<div class="txt-content">';

				// Title
				if ( ($title) && ($html) ) {
					echo "<". $html .">". $title ."</". $html .">";
				}
					
				// Text
				if(!empty(get_field('text'))){
					echo '<p>'.get_field('text').'</p>';
				}

			echo '</div>';

		endif;
		
		// Part 2 : Image 
		echo wp_get_attachment_image($image, 'standard_text_img'); 

	endif; 
	?>

</section>


