<?php
/**
 * Block Name: Texte - 1 paragraphe
 */
 ?>


<?php
$title = get_field('title');
$html = get_field('level_title');
?>

<section class="blk-txt blk-wp wrapper block-pad">

	<?php
	if ( empty($html) ):
		echo '<em>Renseigner le titre</em>';
	else :

		// title
		echo '<'. $html .' class="title">'. $title .'</'. $html .'>';
		
		// text
		if(!empty(get_field('text'))):
			echo '<div class="content">'.the_field('text').'</div>';
		endif;

	endif; 
	?>

</section>
