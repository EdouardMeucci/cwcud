<?php
/**
 * Block Name: Map
 */
 ?>

<?php 
if(is_admin()):
	echo 'Carte des cleanups';
else:
	
	$args = array(
		'paged' => $num_page,
		'post_status'     => 'publish',
		'post_type'			=> 'cleanup',
		'posts_per_page'	=> -1,
	);
	
	
	$args2 = $args;
	$args2['post_status'] = array("private", "publish");
	query_posts($args2);
	global $wp_query; 

	?>
	<!-- Listing Archive -->
	<section>
		<?php echo '<p><b>Il y a '.$wp_query->found_posts.' Cleanups</b></p>';?>
		<form method="GET" class="filters-embassy" action="<?php echo get_permalink(get_field("page_map_cleanup", "option"));?>">
			<!-- Filters -->
			<div class="filters">
				<!-- par départements -->
				<div class="select">
					<select name="s_dep">
					<option value="">Tous les départements</option>
						<?php
						$tab_departements = arrayDepartements();
						foreach ($tab_departements as $key => $value):?>
							<option value="<?php echo $key;?>" <?php if(isset($_GET['s_dep'])) {selected( $key, $_GET['s_dep'] );} ?>><?php echo $key;?> - <?php echo $value;?> </option>
						<?php endforeach;?>
					</select>
				</div>

				<!-- search -->
				<input type="text" placeholder="Rechercher un CyberCleanUp" name="s_cleanup" value="<?php echo (isset($_GET['s_cleanup']))?$_GET['s_cleanup']:'';?>">
			</div>

			<!-- btn -->
			<input type="submit" class="button" value="Rechercher">

		</form>
		
		<!-- wrapper -->
		
		<?php
		
		query_posts($args);
		global $wp_query; 


		if ( have_posts() ) : 
			while (have_posts()) : the_post();
				$post = get_post(get_the_id());
				$author_id = $post->post_author;
				$user = get_userdata( $author_id );
				//var_dump($user);
				$tab_cleanup[] = array(
					'name'		=> get_the_title(),
					'link'		=> get_permalink(),
					'coordonate'=> get_post_meta( $post->ID, "coordonate", true ),
					'etat'		=> get_post_meta( $post->ID, "private", true ),
					'organisator'=> $user->first_name." ".$user->last_name.' - '.get_post_meta($post->ID,'structure_name', true),
					'date' 		=> "le ".date_i18n('j/m', strtotime(get_post_meta( $post->ID, "date_start", true )))." à ".date_i18n('H:i', strtotime(get_post_meta( $post->ID, "time_start", true ))),
				);
			endwhile; 
			?>
			
			<style type="text/css">
				#mapCleanup{ /* la carte DOIT avoir une hauteur sinon elle n'apparaît pas */
					height:650px;
				}
			</style>
			<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin="" />
			<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.css" />
			<!--<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.Default.css" />-->
			
			<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw==" crossorigin=""></script>
			<!--<script type='text/javascript' src='https://unpkg.com/leaflet.markercluster@1.3.0/dist/leaflet.markercluster.js'></script>-->
			
			<script>
				var tab_cleanup = <?php echo json_encode($tab_cleanup);?>;
			</script>
			<div id="mapCleanup"></div> 
			<?php
		else:
			?>
			<p>
				Il n'y a pas de CyberCleanUp correpondant à votre recherche, nous vous invitons à <a href="<?php the_permalink(get_field("page_add_organisateur","option"));?>">créer le premier CyberCleanUp</a> de cette zone.
			</p>
			<?php
		endif;
		wp_reset_query();

	?>

	</div><!-- /wrapper -->

</section><!-- End of Listing Archive -->

<?php endif;?>