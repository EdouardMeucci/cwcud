<?php
/**
 * Block Name: Document téléchargeable
 */
 ?>

<section class="blk-download blk-wp wrapper-small ctr block-pad-top">

<?php
$doc = get_field('download_document');
if ( empty($doc) ):?>

	<em>Renseigner le contenu</em>;
	
<?php else :

	// Image
	if (!empty(get_field('image'))):
		$size = 'logo';
		$image = get_field('image');?>
			<?php echo wp_get_attachment_image($image, $size); ?>
	<?php else:?>
		<img src="<?php echo $doc['icon'];?>" alt="<?php echo $doc['filename'];?>">
		<?php //echo wp_get_attachment_image( $doc['icon'] );
	endif;
	?>

	<div class="txt-content lft">

		<?php
		// Title
		if(!empty(get_field('title'))):
			echo '<h2 class="big">'.get_field('title').'</h2>';
		endif;

		// Filesize
		$size = size_format($doc['filesize']);	

		echo '<p>';
		_e('Taille: ', 'cwcud');
		echo $size;
		echo '</p>';

		// Description
		if(!empty(get_field('description'))):
			echo '<p>'.get_field('description').'</p>';
		endif;?>

		<!-- Button download document -->
		<a class="button" href="<?php echo $doc['url'] ;?>" download="<?php echo $doc['filename'] ;?>"><?php _e('Télécharger', 'cwcud');?></a>

	</div>

<?php //var_dump($doc); ?>



<?php endif; ?>

</section>
