<?php get_header(); ?>

<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<main id="raw-content">
	<?php the_content(); ?>
</main>

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php
get_footer();
