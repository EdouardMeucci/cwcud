<?php
//wp_redirect(home_url(), 301);
?>

<?php get_header(); ?>



<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php the_title(); ?>
<?php the_content(); ?>

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php
get_footer();
